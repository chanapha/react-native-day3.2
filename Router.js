import React, {Component} from 'react'
import {Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import List from './List'
import AddProduct from './AddProduct'
import Product from './Product'
import Profile from './Profile'
import EditProfile from './EditProfile'
import EditProduct from './EditProduct'



import Login from './Login'

class Router extends Component{
    render(){
        return(
            <NativeRouter>
                <Switch>
                    <Route exact path= "/" component= {Login}/>
                    <Route exact path= "/List" component= {List}/>
                    <Route exact path= "/AddProduct" component= {AddProduct}/>
                    <Route exact path= "/Product" component= {Product}/>
                    <Route exact path= "/Profile" component= {Profile}/>
                    <Route exact path= "/EditProfile" component= {EditProfile}/>
                    <Route exact path= "/EditProduct" component= {EditProduct}/>
                    {/* <Redirect to = "/screen1"/> */}
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router