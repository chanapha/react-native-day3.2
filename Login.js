import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image, TextInput, Button,Alert } from "react-native";


export default class Login extends Component{
    state = {
        username : '',
        password : '',
        isLogin : false,
        modelVisible: false
    }

    onValueChange = (field, value) => {
        this.setState ({[field]: value});
    }

    onLogin(){
        const { username, password } = this.state;
        if( username === 'admin' && password === 'eiei'){
          this.setState({ isLogin : true})
          this.props.history.replace('/List', {username: this.state.username, password: this.state.password})
            Alert.alert('YAHOO!')
        }else{
          this.setState({modelVisible: true })
            alert('Username or password is wrong.')
        }
    }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.row}>

            <View style={styles.box1}>
              <View style={styles.box6}>
              <Image source={{uri:'http://123emoji.com/wp-content/uploads/2016/04/1_result1.png'}} style={{ width: 100, height: 100}} />
              </View>
            </View>

            <View style={styles.box2}>
              {/* <Text style={styles}>//</Text> */}

              {/* <Text style = {styles.header}>{this.state.username} Username- Password {this.state.password}</Text> */}

              <View style={styles.box3}>
              <TextInput style= { styles.headerText } value = {this.state.username} placeholder='Username'
              onChangeText= {value => this.onValueChange("username", value)}/>
              
              </View>

              <View style={styles.box3}>
              <TextInput style= { styles.headerText } value = {this.state.password} placeholder='password'
              onChangeText= {value => this.onValueChange("password", value)}/>
              </View>

              <View style={styles.box4}>
                <Text style={styles.headerText2}>TouchableOpacity</Text>
              </View>

            <Button title="Login" onPress={this.onLogin.bind(this)} />

            </View>

          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  header: {
    // backgroundColor: "red",
    // alignItems: "center"
    color: "white",
    fontSize: 20
  },
  headerText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 30,
    textAlign: 'center'
  },
  headerText2: {
    color: "white",
    fontSize: 30,
    textAlign: 'center'
  },

  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "white"
  },
  content: {
    backgroundColor: "yellow",
    flex: 1,
    flexDirection: "row"
  },
  box1: {
    backgroundColor: "#808080",
    flex: 1,
    alignItems: 'center'
  },
  box2: {
    backgroundColor: "#808080",
    flex: 1
  },
  box3: {
    backgroundColor: "#DCDCDC",
    width: 350, 
    height: 50,
    margin: 30

  },
  box4: {
    backgroundColor: "#000000",
    width: 350, 
    height: 50,
    margin: 30
  },
  box5: {
    backgroundColor: "pink",
    width: 350, 
    height: 200,
    margin: 30
  },
  box6: {
    backgroundColor: "white",
    borderRadius: 100,
    width: 200, 
    height: 200,
    margin: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },


  row: {
    backgroundColor: "cyan",
    flex: 1,
    flexDirection: "column"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
