import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView
} from "react-native";

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ flex: 1 }}>

        <View style={styles.header}>
        <TouchableOpacity onPress = {() => this.props.history.replace('/List')}>
            <View style={styles.header}>
                <Text style={styles.headerText}>{'<'}</Text>
            </View>
        </TouchableOpacity>
            <Text style={styles.headerText}>My Profile</Text>
        </View>
             <View>
                <Text style={styles.labelText}> User name </Text>
            </View>
            <View>
                <Text style={styles.labelText}> First name </Text>
            </View>
            <View>
                <Text style={styles.labelText}> Last name  </Text>
            </View>



        <View style={styles.footerContainer}>
        <TouchableOpacity onPress = {() => this.props.history.replace('/EditProfile')}>
          <View style={styles.box4}>
            <Text style={styles.headerText2}>Edit Profile</Text>
          </View>
        </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = {
  footerContainer: {
    flexDirection: "row",
    position: "absolute",
    bottom: 0,
    left: 0,
    width: WIDTH,
    height: HEIGHT / 10
  },
  box4: {
    backgroundColor: "#000000",
    width: 350,
    height: 50,
    margin: 30
  },
  headerText2: {
    color: "white",
    fontSize: 30,
    textAlign: "center"
  },
  header: {
    backgroundColor: "red",
    alignItems: "center",
    flexDirection: 'row'
  },
  headerText: {
    color: "white",
    fontWeight: "bold",
    padding: 30,
    fontSize: 30
  },
  labelText: {
    color: "black",
    fontWeight: "bold",
    padding: 30,
    fontSize: 30

  }
};
