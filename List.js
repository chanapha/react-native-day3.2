import React, { Component } from "react";
import { Text, View, TouchableOpacity, Dimensions, Image, ScrollView } from "react-native";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default class List extends Component {
  constructor(props){
      super(props);
      this.state = {
          imagesUri: [
            'http://sdl-shop.line.naver.jp/themeshop/v1/products/xx/xx/xx/41a04421-2a7c-456b-8fed-0cb8d1ebd23b/1/ANDROID/th/preview_001_720x1232.png', 'http://sdl-shop.line.naver.jp/themeshop/v1/products/xx/xx/xx/f1196af0-7301-4689-9611-001749f01827/1/ANDROID/th/preview_001_720x1232.png',
            'https://1.bp.blogspot.com/-pzsb1ilbNRs/WkT8hSow4HI/AAAAAAAWg54/Mmulkl0OZMUiqpvICzTip2M3kK8pRmZ-ACLcBGAs/s1600/C081769_1.png', 'https://uploads.guim.co.uk/2017/10/06/First-Dog-on-the-Moon,_L.png',
            'https://png.pngtree.com/element_our/png_detail/20181031/cute-unicorn-vector-object-background-png_225166.jpg' ,'https://i.pinimg.com/originals/2c/f2/77/2cf277a7a5c4345f90412603fd535960.jpg',
            'https://image.shutterstock.com/image-vector/vector-cartoon-unicorn-girl-pink-450w-1052728469.jpg', 'https://i.pinimg.com/originals/c0/20/4d/c0204dede54218a0397a0d047175ff5e.jpg', 
            'https://1.bp.blogspot.com/-9B8ysX6jBho/WVPB55fsi5I/AAAAAAAInfc/hauOeh9z9DcappH-H2_q-Q4n5SrQn2_PgCLcBGAs/s1600/C050823_1.png', 'https://lh3.googleusercontent.com/KwI1i646qosNfmo8ACpL5MfCFMKHT_S4im3gOmFdY3H5jHXNi7LDALHDIGMye-gAAjA',
            'http://sdl-shop.line.naver.jp/themeshop/v1/products/xx/xx/xx/41a04421-2a7c-456b-8fed-0cb8d1ebd23b/1/ANDROID/th/preview_001_720x1232.png', 'https://shop.line-scdn.net/themeshop/v1/products/82/24/6c/82246c51-1983-4f49-8bd5-93b9a903b697/24/WEBSTORE/icon_198x278.png?__=20161019'
        ]
      }
  }
  render() {
    const { container, footerContainer, listButtonContainer, addButtonContainer, profileButtonContainer, centeredTextContainer, labelText, headerContainer, imageContainer: rowContainer, imageStyle, contentContainer } = styles;
    const { imagesUri } = this.state; 
    return (
    <View style={{flex: 1}}>
      <ScrollView style={container}>
        <View style={contentContainer}>
            <View style={rowContainer}>
                <Image style={imageStyle} source={{ uri: imagesUri[0]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[1]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[2]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[3]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[4]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[5]}} />
               
             </View>
             <View style={rowContainer}>
                <Image style={imageStyle} source={{ uri: imagesUri[6]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[7]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[8]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[9]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[10]}} />
                <Image style={imageStyle} source={{ uri: imagesUri[11]}} />
             </View>
        </View>
      </ScrollView>
      <View style={[headerContainer, centeredTextContainer]}>
            <Text style={labelText}>List</Text>
      </View>
       <View style={footerContainer}>
       <TouchableOpacity style={[listButtonContainer, centeredTextContainer, { width: 1 * WIDTH / 5 }]}>
           <Text style={labelText}>List</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={() => this.props.history.replace('/AddProduct')} style={[addButtonContainer, centeredTextContainer, { width: 3 * WIDTH / 5 }]}>
           <Text style={labelText}>Add</Text>
       </TouchableOpacity>
       <TouchableOpacity style={[profileButtonContainer, centeredTextContainer, { width: 1 * WIDTH / 5 }]} onPress={() => { this.props.history.replace('/Profile') }}>
           <Text style={labelText}>Profile</Text>
       </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = {
    container: {
        flex: 1, marginBottom: HEIGHT / 10, marginTop: HEIGHT / 10
    },
    headerContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: WIDTH,
        height: HEIGHT / 10,
        backgroundColor: 'grey'
    },
    footerContainer: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        height: HEIGHT / 10
    },
    centeredTextContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    labelText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white'
    },
    listButtonContainer: {
        backgroundColor: 'blue',
    },
    addButtonContainer: {
        backgroundColor: 'yellow'
    },
    profileButtonContainer: {
        backgroundColor: 'red'
    },
    contentContainer: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    rowContainer: {
        flex: 1, padding: 50
    },
    imageStyle: {
        width: HEIGHT / 6, height: HEIGHT / 6
    }

}
